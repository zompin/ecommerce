;(function($) {
	'use strict';
	var products = [];
	var basket = Basket($.cookie('basket'));

	$(document).ready(function() {
		loadPage(location.hash.substr(1));
		$('.main-menu__item').click(mainNav);
		$('.hero__button').click(hero);
		$('.search__input').keyup(productSearch);
		$('.main').delegate('.product__size-change', 'click', changeSize);
		$('.main').delegate('.add-to-basket', 'click', addTobasket);
		$('.main').delegate('.basket__item-remove', 'click', removeFromBasket);
		$('.main').delegate('.basket__item-amount', 'keyup', changeItemAmount);
		$('.main').delegate('.checkout', 'click', checkout);
		renderBasket();
	});

	function loadPage(path) {
		var url = 'products.php';
		showLoading();

		if (path) {
			url += '?page=' + path;
			$('.main-menu').find('a[href="#' + path + '"]').addClass('main-menu__item_current');
		} else {
			$('.main-menu__item').first().addClass('main-menu__item_current');
		}

		if (path == 'BASKET') {
			renderBasket();
			return;
		}

		$.ajax({
			url: url,
			dataType: 'json',
			success: function(data) {
				renderProducts(data, true);
			}
		});
	}

	function mainNav() {
		$('.main-menu__item_current').removeClass('main-menu__item_current');
		loadPage($(this).attr('href').substr(1));
	}

	function showLoading() {
		var loading = $('.loading');

		if (loading.length == 0) {
			$('.main').html('<div class="loading"></div>');
		} else {
			$(loading).remove();
		}
	}

	function renderProducts(data, cache) {
		var html = '';

		if (cache == true) {
			products = data;
			
			for (var i = 0; i < data.length; i++) {
				data[i].id = i;
				data[i].picture = 'imgs/product-image.png';
			}
		}

		if (data instanceof Array) {
			for (var i = 0; i < data.length; i++) {
				html += renderProductItem(data[i]);
			}
			if (data.length == 0) {
				html = '<div class="no-products">No products</div>';
			}
		} else {
			html = data;
		}

		$('.main').html(html);
	}

	function renderProductItem(item) {
		var html = '<div class="product" data-product-id="' + item.id + '">' +
						'<div class="product__inner">' +
							'<div class="product__image">' +
								'<img src="' + item.picture + '" alt="' + item.name + '">' +
							'</div>' +
							'<div class="product__desc">' +
								'<div class="product__name">' + item.name + '</div>' +
								'<div class="product__color">' + item.color + '</div>' +
								'<div class="product__size">Size: ' +
									'<span class="product__size-value">' + item.size + '</span>' +
									'<a href="#" class="product__size-change">[Change]</a>' +
								'</div>' +
								'<div class="product__price">' +
									'<span class="currency">£</span><span class="producn__price-value">' + item.price.toFixed(2) + '</span>' +
								'</div>' +
								'<button class="button product__button add-to-basket">ADD TO BASKET</button>' +
							'</div>' +
						'</div>' +
					'</div>';

		return html;
	}

	function hero() {
		alert('Hero');
	}

	function productSearch() {
		var phrase = $(this).val().toLowerCase();
		var sortedProducts = [];

		if (phrase === '') {
			renderProducts(products);
		} else {
			for (var i = 0; i < products.length; i++) {

				if (products[i].name.toLowerCase().indexOf(phrase) >= 0) {
					sortedProducts.push(products[i]);
				}
			}

			if (sortedProducts.length == 0) {
				sortedProducts = '<div class="no-products">Products not found</div>';
				console.log('string')
			}

			renderProducts(sortedProducts);
		}
	}

	function changeSize(e) {
		alert('Change size');
		e.preventDefault();
	}

	function addTobasket() {
		var id = $(this).parent().parent().parent().attr('data-product-id');

		for (var i = 0; i < products.length; i++) {
			if (products[i].id == id) {
				basket.add(products[i]);
				break;
			}
		}

		alert('Added to basket');
	}

	function removeFromBasket() {
		var id = $(this).parent().attr('data-basket-item-id');
		basket.remove(id);
		renderBasket();
	}

	function changeItemAmount() {
		var amount = $(this).val();
		var basketItem = $(this).parent();
		var id = basketItem.attr('data-basket-item-id');

		basket.changeAmount(id, amount);

		var item = basket.getItem(id);

		basketItem.find('.basket__item-price-value').html(item.price * item.amount);
		$('.basket__total-price').html(basket.getTotalPrice());
	}

	function Basket(items) {
		var basket = [];
		if (items) {
			items = JSON.parse(items);

			if (items instanceof Array) {
				basket = items;
			}
		}

		function updateCookie() {
			$.cookie('basket', JSON.stringify(basket));
		}

		return {
			add: function(item) {
				var itemFound = false;

				for (var i = 0; i < basket.length; i++) {
					if (item.id == basket[i].id) {
						basket[i].amount++;
						itemFound = true;
						break;
					}
				}

				if (!itemFound) {
					item.amount = 1;
					basket.push(item);
				}

				updateCookie();
			},
			remove: function(id) {
				for (var i = 0; i < basket.length; i++) {
					if (id == basket[i].id) {
						basket.splice(i, 1);
						break;
					}
				}

				updateCookie();
			},
			changeAmount: function(id, amount) {
				for (var i = 0; i < basket.length; i++) {
					if (id == basket[i].id) {
						basket[i].amount = amount * 1;
						break;
					}
				}

				updateCookie();
			},
			getItems: function() {
				return basket;
			},
			getItem: function(id) {
				for (var i = 0; i < basket.length; i++) {
					if (id == basket[i].id) {
						return basket[i];
					}
				}
			},
			getTotalPrice: function() {
				var total = 0;

				for (var i = 0; i < basket.length; i++) {
					total += basket[i].price * basket[i].amount;
				}

				return total;
			}
		}
	}

	function renderBasket() {
		var items = basket.getItems();
		var html = '';

		for (var i = 0; i < items.length; i++) {
			html += renderBasketItem(items[i]);
		}

		if (!html) {
			html = '<div class="basket__empty">Basket is empty</div>';
		}

		html = '<div class="basket">' +
					'<div class="basket__header">Shopping Bag</div>' +
					'<div class="basket__items">' + html + '</div>' +
					'<div class="basket__footer">' +
						'<div class="basket__total">' +
							'Total: ' +
							'<span class="currency">£</span><span class="basket__total-price">' + basket.getTotalPrice() + '</span>' +
						'</div>' +
						'<button class="button basket__button checkout">CHECKOUT</button>' +
					'</div>' +
				'</div>';
		$('.main').html(html);
	}

	function renderBasketItem(item) {
		var html = '<div class="basket__item" data-basket-item-id="' + item.id + '">' +
						'<div class="basket__item-image">' +
							'<img src="' + item.picture + '" class="basket__img" alt="' + item.name + '">' +
						'</div>' +
						'<div class="basket__item-name">' + item.name + '</div>' +
						'<a href="#" class="basket__item-remove">[Remove]</a>' +
						'<input type="text" class="input basket__item-amount" value="' + item.amount + '">' +
						'<div class="basket__item-price">' +
							'<span class="basket__item-price-currency">£</span><span class="basket__item-price-value">' + item.price * item.amount+ '</span>' +
						'</div>' +
					'</div>';

		return html;
	}

	function checkout() {
		alert('Checkout');
	}
})(jQuery);