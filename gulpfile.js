var gulp = require('gulp');
var less = require('gulp-less');
var bs = require('browser-sync').create();

gulp.task('default', ['bs', 'less'], function() {
	gulp.watch('less/*.less', ['less']);
});

gulp.task('less', function() {
	gulp.src('less/*.less').pipe(less()).pipe(gulp.dest('less/../'));
});

gulp.task('bs', function() {
	bs.init({
		//server: './',
		proxy: 'ecommerce',
		notify: false,
		files: ['*.html', '*.css', 'js/*.js']
	});
});